import React from 'react';

export const ListItem = ({ list }) => <li>{list.title}</li>;

export default (lists) => (
  <ul>
    { lists.length && lists.map(list => <ListItem list={list} />)}
    { !lists.length && <li>There are no lists yet.</li> }
  </ul>
)
