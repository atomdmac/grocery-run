import * as React from 'react'
import { useState } from 'react'
import { tags } from './constants'
import { ITag } from './interfaces'
import './List.css'
import './TagList.css'

interface ISelectableTag extends ITag {
  selected: boolean
}

const TagList = ({
  handleUpdateTags,
  selectedTags = []
}: {
  handleUpdateTags: (tags: ITag[]) => void,
  selectedTags?: ITag[]
}) => {
  const [
    selectableTags,
    setSelectableTags
  ]: [
    ISelectableTag[],
    Function
  ] = useState(tags.map(tag => ({
      selected: !!selectedTags.find(st => st.title === tag),
      title: tag
    }))
  )

  const createTagId = (tag) =>
    `tag_${tag.title.toLowerCase().replace(/[\s\/\\_]/, '')}`

    const toggleTag = (selectableTag: ISelectableTag) => {
      const newSelectableTags = selectableTags.map(
        tag => tag.title === selectableTag.title
          ? { ...selectableTag, selected: !selectableTag.selected }
          : tag
      )
      setSelectableTags(newSelectableTags)
      handleUpdateTags(
        newSelectableTags
          .filter(sTag => sTag.selected)
          .map(sTag => ({ title: sTag.title }))
      )
    }

  return (
    <div className="tag-filter">
      <ul className="list">
        {
          selectableTags.map(tag =>
                         <li key={ tag.title }>
                           <input
                             id={createTagId(tag)}
                             type="checkbox"
                             checked={ tag.selected }
                             onChange={ () => toggleTag(tag) }
                           />
                           <label htmlFor={createTagId(tag)}>
                             { tag.title }
                           </label>
                         </li>
                        )
        }
      </ul>
    </div>
  )
}

export default TagList
