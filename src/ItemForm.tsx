import React, { useState, SyntheticEvent } from 'react'
import CurrencyInput from 'react-money-input'
import './ItemForm.css'

import TagList from './TagList'

import { IListItemUpdate, ITag } from './interfaces'

const ItemForm = ({
  initialTitle = '',
  initialPrice = 0,
  initialTags = [],
  handleSubmit
}: {
  initialTitle?: string,
  initialPrice?: number,
  initialTags?: ITag[],
  handleSubmit(itemUpdate: IListItemUpdate): void
}) => {
  const [ title, setTitle ] = useState(initialTitle)
  const [ price, setPrice ] = useState(initialPrice)
  const [ tags, setTags ] = useState(initialTags)

  const handleSubmitButton = () => {
    handleSubmit({
      price,
      tags,
      title
    })
    setTitle('')
    setPrice(0)
    setTags([])
  }

  const handleKeyPress = (event: any) => {
    if (event.key === 'Enter') {
      handleSubmitButton()
    }
  }

  return (
    <div className="ItemForm">
      <div className="row">

        <input
          name="item-name"
          type="text"
          placeholder="📦"
          value={ title }
          onKeyPress={ handleKeyPress }
          onChange={ e => setTitle(e.target.value) }
        />
        <CurrencyInput
          name="item-price"
          currency="USD"
          currencyDisplay="symbol"
          locale="us-US"
          initialValue={ price }
          onKeyPress={ handleKeyPress }
          onChange={ (event: SyntheticEvent, value: number) => setPrice(value) }
        />
        <button
          onClick={ handleSubmitButton }
        >👍</button>
      </div>
      <TagList selectedTags={ tags } handleUpdateTags={ setTags }/>
    </div>
  )
}

export default ItemForm
