import * as React from 'react'
import { Component, useContext, useEffect } from 'react'
import { BrowserRouter as Router, NavLink, Route } from 'react-router-dom'
import './App.css';

import AppProvider, { Context as StoreContext }from './AppProvider'
import ItemCreator from './ItemCreator'
import ItemEditor from './ItemEditor'
import ShoppingList from './ShoppingList'

const App = () => {
  const {
    actions,
    callApi,
    dispatch,
    effects,
    showConfirm,
    state
  } = useContext(StoreContext)

  return (
    <div className="content">
      { state.list.isLoading
        ? <div className="loading-message"><span>🕐</span></div>
        : <ShoppingList />
      }
    </div>
  );
}

const AppWrapper = () => {
  const {
    actions,
    showConfirm,
    dispatch,
    effects,
    state,
    callApi
  } = useContext(StoreContext)

  const doLoad = async () => {
    dispatch(actions.loadList())
    const result = await effects.loadList()
    dispatch(actions.loadListSuccess(result))
  }

  // Do initial load
  useEffect(() => {
    doLoad()
  }, [])

  return (
    <Router>
      <div className="App">
        <div className="content-wrapper">
          <nav>
            <ul>
              <li style={{ margin: 0, flexGrow: 1 }}>
                <span style={{ fontSize: '0.65em'  }}>🛒</span>🏃
              </li>
              <li>
                <NavLink
                  to="/"
                  activeClassName="active"
                  exact={true}>
                  List
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/item/add"
                  activeClassName="active">
                  Add
                </NavLink>
              </li>
            </ul>
          </nav>
          <hr/>
          <Route path="/" exact={true} component={App} />
          <Route path="/item/add" component={ ItemCreator } />
          <Route path="/item/edit/:itemId" component={ ItemEditor } />
        </div>
      </div>
    </Router>
  )
}

class AppContainer extends Component {
  public render() {
    return (
      <AppProvider>
        <AppWrapper />
      </AppProvider>
    )
  }
}

export default AppContainer
