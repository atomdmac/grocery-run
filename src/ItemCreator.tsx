import React, { useContext, useEffect, useState } from 'react'
import { Context as StoreContext } from './AppProvider'
import { IListItemUpdate } from './interfaces';
import ItemForm from './ItemForm'
import { actions } from './store/actions'

const ItemCreator = () => {
  const {
    dispatch,
    state
  } = useContext(StoreContext)

  const handleAddItem = (itemUpdate: IListItemUpdate) => {
    dispatch(actions.addNewListItem(state.list.id, itemUpdate))
  }

  return (
    <ItemForm handleSubmit={ handleAddItem } />
  )
}

export default ItemCreator
