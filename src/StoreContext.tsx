import * as React from 'react'
import { createContext, useReducer } from 'react'

const StoreContext = createContext(null)

const StoreProvider = ({
  actions, reducer, initialState, children
}) => {
  const [ state, dispatch ] = useReducer(reducer, initialState)
  return (
    <StoreContext.Provider value={{ state, actions, dispatch }}>
      { children }
    </StoreContext.Provider>
  )
}

export default {
  Consumer: StoreContext.Consumer,
  Provider: StoreProvider
}
