import React, { useContext } from 'react'
import { Context as StoreContext } from './AppProvider'
import { IListItem, IListItemUpdate } from './interfaces';
import ItemForm from './ItemForm'

const ItemEditor = ({ history, match: { params } }) => {
  const {
    state,
    actions,
    dispatch
  } = useContext(StoreContext)

  const item = state.list.items.find((i: IListItem) => i.id === params.itemId)

  const handleUpdateItem = (itemUpdate: IListItemUpdate) => {
    dispatch(actions.updateListItem({
      id: item.id,
      ...itemUpdate
    }))
    history.goBack()
  }

  return (
    item
      ? <ItemForm
        handleSubmit={ handleUpdateItem }
        initialPrice={ item.price }
        initialTitle={ item.title }
        initialTags={ item.tags } />
      : <p>I bet you'd love to edit item { params.itemId }</p>
  )
}

export default ItemEditor
