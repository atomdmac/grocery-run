import * as React from 'react'
import { useState } from 'react'

import './Collapsible.css'

const Collapsible = ({ children, title, collapsed = true}) => {
  const [ isCollapsed, setIsCollapsed ] = useState(collapsed)
  return (
    <div className="collapsible">
      <h2 onClick={ () => setIsCollapsed(!isCollapsed) }>
        { title } [ { isCollapsed ? '+' : '-' } ]
      </h2>
      { !isCollapsed && children }
    </div>
  )
}

export default Collapsible
