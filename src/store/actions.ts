import {
  actionNames as listActionNames,
  actions as listActions
} from './modules/list'

export const getAction = (creatorOrAction: (params: any) => void | any, params: any) => {
  switch(typeof creatorOrAction) {
    case 'function':
      return creatorOrAction(params)
    case 'object':
      return creatorOrAction
    default:
      return { type: 'noop' }
  }
}

export const actionNames = {
  ...listActionNames
}

export const actions = {
  ...listActions
}

