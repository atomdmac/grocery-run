import { combineReducers } from 'redux'
import { reducer as list } from './modules/list'

export const reducer = combineReducers({ list })
