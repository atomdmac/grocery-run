import * as React from 'react'
import { useEffect, useState } from 'react'
import { getAction } from './actions'

// TODO: Find a better way to mock the built-in `fetch` method
const fetch = (url: string, options: any) => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(options.body || {}), Math.random() * 2000)
  })
}

export const useApi = (dispatch) => {
  const [ url, setUrl ] = useState('')
  const [ requestOptions, setRequestOptions ] = useState(null)
  const [ successAction, setSuccessAction ] = useState(null)
  const [ failAction, setFailAction ] = useState(null)

  useEffect(() =>  {
    if (!successAction || !failAction) {
      console.log('No action to call after API returns.  Bailing. 👋')
      return
    }

    fetch(url, requestOptions)
      .then((response) => dispatch(getAction(successAction, response)))
      .catch((error) => dispatch(getAction(failAction, error)))
  }, [url])

  const callApi = (
    newUrl: string,
    newSuccessAction,
    newFailAction,
    newRequestOptions = {}
  ) => {
    setUrl(newUrl)
    setSuccessAction(newSuccessAction)
    setFailAction(newFailAction)
    setRequestOptions(newRequestOptions)
  }

  return callApi
}

export const useConfirm = (dispatch) => {
  const [ id, setId ] = useState('')
  const [ message, setMessage ] = useState('')
  const [ successAction, setSuccessAction ] = useState(null)
  const [ failAction, setFailAction ] = useState(null)

  useEffect(() => {
    if (!id.length) {
      return
    }
    const response = confirm(message)
    if (response) {
      dispatch(getAction(successAction, response))
    } else {
      dispatch(getAction(failAction, response))
    }
  }, [ id ])

  return (newMessage, newSuccessAction, newFailAction) => {
    setId('' + Math.random())
    setSuccessAction(newSuccessAction)
    setFailAction(newFailAction)
    setMessage(newMessage)
  }
}

