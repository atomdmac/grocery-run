import { actionNames } from './actions'
import ApiLocal from '../api/api.local'
import { IListData, IListItem, IListItemData } from '../interfaces';

const api = new ApiLocal(true)

export const effects = {
  [actionNames.LOAD_LIST]: async () => {
    // TODO: Actually support multiple lists.
    const lists: IListData[] = await api.getAllLists()
    const listItems: IListItemData[] = await api.getListItems(lists[0].id)

    return listItems
  },
  [actionNames.LOAD_ALL_LISTS]: async () => {
    await api.getAllLists()
  },
  [actionNames.LOAD_LIST_ITEMS]: async (listId) => {
    await api.getListItems(listId)
  }
  // Load success / failure
  // [actionNames.ADD_NEW_LIST]: (list: IListData) => {},
  // [actionNames.REMOVE_LIST]: (listId: string) => {},
  // [actionNames.UPDATE_LIST]: (listId: string, data: IListData) => {},
  // [actionNames.ADD_NEW_LIST_ITEM]: (item: IListItemData) => {},
  // [actionNames.REMOVE_LIST_ITEM]: (itemId: string) => {},
  // [actionNames.UPDATE_LIST_ITEM]: (itemId: string, data: IListItemData) => {}
}
