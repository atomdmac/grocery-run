import update from 'immutability-helper'
import {
  IAction,
  IList,
  IListItem,
  IListItemData,
  IListItemUpdate,
  IListUpdate,
  ITag,
  IListData,
} from '../../interfaces'

// `uuid` doesn't want to be `import`ed by Typescript 🤔
const uuid = require('uuid')

export interface IListState {
  id: string
  isInvalidated: boolean
  isLoading: boolean
  items: IListItem[]
  title: string
}

export const actionNames = {
  ADD_NEW_LIST: 'addNewList',
  ADD_NEW_LIST_ITEM: 'addNewListItem',
  LOAD_ALL_LISTS: 'loadAllLists',
  LOAD_ALL_LISTS_SUCCESS: 'loadAllListsSuccess',
  LOAD_LIST: 'loadList',
  LOAD_LIST_ITEMS: 'loadListItems',
  LOAD_LIST_ITEMS_SUCCESS: 'loadListItemsSuccess',
  LOAD_LIST_SUCCESS: 'loadListSuccess',
  REMOVE_LIST_ITEM: 'removeListItem',
  UPDATE_LIST: 'updateList',
  UPDATE_LIST_ITEM: 'updateListItem'
}

export const actions = {
  [actionNames.LOAD_LIST]: () => ({
    type: actionNames.LOAD_LIST
  }),
  [actionNames.LOAD_LIST_SUCCESS]: (data) => ({
    data,
    type: actionNames.LOAD_LIST_SUCCESS
  }),
  // Loading Lists
  [actionNames.LOAD_ALL_LISTS]: () => ({
    type: actionNames.LOAD_ALL_LISTS
  }),
  [actionNames.LOAD_ALL_LISTS_SUCCESS]: (lists: IListData) => ({
    lists,
    type: actionNames.LOAD_ALL_LISTS_SUCCESS
  }),

  // Loading List Items
  [actionNames.LOAD_LIST_ITEMS]: () => ({
    type: actionNames.LOAD_LIST_ITEMS
  }),
  [actionNames.LOAD_LIST_ITEMS_SUCCESS]: (listItems: IListItemData) => ({
    listItems,
    type: actionNames.LOAD_LIST_ITEMS_SUCCESS
  }),

  // [actionNames.ADD_NEW_LIST]: (list: IListUpdate) => ({}),
  // [actionNames.UPDATE_LIST]: (listId: string, data: IListUpdate) => ({}),
  [actionNames.ADD_NEW_LIST_ITEM]: (listId: string, itemUpdate: IListItemUpdate) => {
    return {
      itemUpdate,
      listId,
      type: actionNames.ADD_NEW_LIST_ITEM
    }
  },
  [actionNames.REMOVE_LIST_ITEM]: (itemId: string) => ({
    itemId,
    type: actionNames.REMOVE_LIST_ITEM
  }),
  [actionNames.UPDATE_LIST_ITEM]: (itemUpdate: IListItemUpdate) => {
    return ({
      itemUpdate,
      type: actionNames.UPDATE_LIST_ITEM
    })
  }
}

const updateListItem = (state: IListState, action: IAction): IListState => {
  return update(state, {
    items: {
      $set: state.items.map(
        (item: IListItem) => item.id === action.itemUpdate.id
          ? { ...item, ...action.itemUpdate }
          : item
        )
    }
  })
}

const addNewListItem = (
  state: IListState,
  action: any
): IListState => {
  // UI representation of item
  const {
    title,
    price,
    tags
  } = action.itemUpdate

  const newItem: IListItem = {
    completed: false,
    id: uuid(),
    isInvalidated: false,
    price,
    tags,
    title
  }

  return update(state, {
    items: {
      $push: [newItem]
    }
  })
}

const removeListItem = (
  state: IListState,
  action: IAction
): IListState => {
  const itemIndex = state.items.findIndex(
    item => item.id === action.itemId
  )

  if (itemIndex < 0) {
    throw new Error(`Item with ID ${action.itemId} cannot be removed because it's not in the list`)
  }

  return update(state, {
    items: {
      $splice: [[itemIndex, 1]]
    }
  })

  // await api.removeListItem(listId, itemId)
  // TODO: Deal with remove failures
}

const toggleItemCompletion = async (
  state: IListState,
  action: IAction
): Promise<IListState> => {
    const item = state.items.find(i => i.id === action.itemId);
    if (!item) {
      throw new Error(`No item found with ID ${action.itemId}`)
    }

    return updateListItem(state, {
      ...action,
      itemUpdate: {
        completed: !item.completed,
        id: item.id
      }
    })
}

export const initialState: IListState = {
  id: null,
  isInvalidated: true,
  isLoading: false,
  items: [],
  title: null
}

export const reducer = (state: IListState = initialState, action: IAction) => {
  switch(action.type) {
    case actionNames.LOAD_LIST:
      return update(state, {
        isLoading: {
          $set: true
        }
      })
    case actionNames.LOAD_LIST_SUCCESS:
      console.log('DATA: ', action.data)
      return update(state, {
        isLoading: {
          $set: false
        },
        items: {
          $set: action.data
        },
        title: {
          $set: action.data.title
        }
      })
    case actionNames.ADD_NEW_LIST_ITEM:
      return addNewListItem(state, action)
    case actionNames.REMOVE_LIST_ITEM:
      return removeListItem(state, action)
    case actionNames.UPDATE_LIST_ITEM:
      return updateListItem(state, action)
    default:
      return state
  }
}
