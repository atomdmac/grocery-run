import { IListData, IListItemData } from '../interfaces'
import FakeItemGenerator from './FakeItemGenerator'

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
const delayRandom = () => delay(Math.random() * 1000)

const fakeFetch = () => delay(500)

// UUID doesn't like the import syntax for some reason 🤷
const uuid = require('uuid');

class LocalStore {
  private lists: IListData[]
  private listItems: IListItemData[]

  constructor(createFixtures: boolean = false) {
    if (createFixtures) {
      const {
        listItems,
        lists
      } = FakeItemGenerator.generateData()

      this.saveListItems(listItems)
      this.saveLists(lists)
    }
    this.lists = this.loadLists()
    this.listItems =  this.loadListItems()
  }

  public async getListItems(listId: string): Promise<any> {
    return fakeFetch()
      .then(() => {
        const list = this.lists.find(l => l.id === listId)
        if (list) {
          return this.listItems.filter(li => list.items.includes(li.id))
        }
      })
  }

  public async getAllLists(): Promise<IListData[]> {
    return fakeFetch().then(() => this.lists)
  }

  public async createListItem(
    listId: string,
    itemData: IListItemData
  ): Promise<IListItemData> {
    return fakeFetch()
      .then(() => {
        const list = this.lists.find(l => l.id === listId)
        if (list) {
          const newListItem = {
            ...itemData,
            id: uuid.v4()
          }
          list.items.push(newListItem.id)
          this.listItems.push(newListItem)
          return Promise.resolve(newListItem)
        }
        return Promise.reject(new Error('Didnt work.'))
      })
  }

  public async updateListItem(
    itemData: IListItemData
  ): Promise<IListItemData> {
    return fakeFetch()
    .then(() => {
      const itemIndex = this.listItems.findIndex(i => i.id === itemData.id)
      if (itemIndex > -1) {
        this.listItems[itemIndex] = {
          ...this.listItems[itemIndex],
          ...itemData
        }
        return Promise.resolve(this.listItems[itemIndex])
      }
      Promise.reject()
    })
  }

  public loadListItems(): IListItemData[] {
    const listItems = localStorage.getItem('listItems');
    return listItems
      ? JSON.parse(listItems)
      : []
  }

  public saveListItems(itemData: IListItemData[]) {
    localStorage.setItem('listItems', JSON.stringify((itemData)))
  }

  public loadLists(): IListData[] {
    const lists = localStorage.getItem('lists')
    return lists
      ? JSON.parse(lists)
      : []
  }

  public saveLists(listData: IListData[]) {
    localStorage.setItem('lists', JSON.stringify(listData))
  }
}

export default LocalStore
