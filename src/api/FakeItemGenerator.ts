import { IListData, IListItemData, ITag } from '../interfaces'

const randomInt = (min: number, max: number) =>
  Math.round((Math.random() * (max - min)) + min)

const randomItem = (array: any[]) =>
  array[randomInt(0, array.length - 1)]

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
const delayRandom = () => delay(Math.random() * 1000)

// UUID doesn't like the import syntax for some reason 🤷
const uuid = require('uuid');

interface IFakeItemData {
  lists: IListData[]
  listItems: IListItemData[]
}

class FakeItemGenerator {
  public static generateData(): IFakeItemData {
    const lists = FakeItemGenerator.generateLists(10)
    const listItems = []

    lists.forEach(list => {
      FakeItemGenerator.generateListItems(10)
        .forEach(listItem => {
          listItems.push(listItem)
          list.items.push(listItem.id)
        })
    })

    return {
      listItems,
      lists
    }
  }

  public static generateLists(amount: number = 1): IListData[] {
    const lists: IListData[] = []

    for(let l = 0; l < amount; l++) {
      lists.push({
        id: uuid.v4(),
        items: [],
        title: `List ${l}`
      })
    }

    return lists
  }

  public static generateListItems(amount: number = 1): IListItemData[] {
    const listItems: IListItemData[] = []

    for(let l = 0; l < amount; l++) {
      listItems.push({
        completed: false,
        id: uuid.v4(),
        price: randomInt(0, 100),
        tags: [randomItem(FakeItemGenerator.tags)],
        title: `Item ${l}`
      })
    }

    return listItems
  }

  private static tags: ITag[] = [
    { title: 'Product' },
    { title: 'Meat/Poultry' },
    { title: 'Natural Foods' },
    { title: 'Snacks' }
  ]
}

export default FakeItemGenerator
