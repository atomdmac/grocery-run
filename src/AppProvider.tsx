import * as React from 'react'
import { createContext, useReducer, useState } from 'react'
import { effects } from './store/effects'
import { useApi, useConfirm } from './store/hooks'
import { actions as listActions } from './store/modules/list'
import { reducer } from './store/reducer'

export const Context = createContext(null)

const AppProvider = ({ children }) => {
  const [ state, dispatch ] = useReducer(reducer, reducer(undefined, {type: 'init'}))
  const callApi = useApi(dispatch)
  const showConfirm = useConfirm(dispatch)

  const value = {
    actions: {
      ...listActions
    },
    callApi,
    dispatch,
    effects,
    showConfirm,
    state
  }

  return (
    <Context.Provider value={value}>
      { children }
    </Context.Provider>
  )
}

export default AppProvider
