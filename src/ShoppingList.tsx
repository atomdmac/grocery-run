import * as React from 'react'
import { useContext, useState } from 'react'
import { Link } from 'react-router-dom'
import './ShoppingList.css'

import { Context as StoreContext }from './AppProvider'
import Collapsible from './Collapsible'
import TagList from './TagList'

import { IList, IListItem, ITag } from './interfaces'

const ShoppingListItem = ({
  item,
  removeItem,
  toggleItemCompletion
}: {
  item: IListItem,
  removeItem(item: IListItem): void,
  toggleItemCompletion(item: any): void
},
) => (
  <li key={ item.id }>
    <input
      id={ `shopping_list_item_${item.id}` }
      type="checkbox"
      checked={ item.completed }
      disabled={ item.isInvalidated }
      onChange={ () => toggleItemCompletion(item) }
    />
    <label
      className="item-name"
      htmlFor={`shopping_list_item_${item.id}`}
    >{item.title}</label>
    <label
      className="item-price"
      htmlFor={`shopping_list_item_${item.id}`}
    >${item.price}</label>
    { item.isInvalidated && <span role="img" aria-label="invalidated">❗</span> }
    <Link to={ `/item/edit/${item.id}` }>✏️</Link>
    <button onClick={ () => removeItem(item) }>❌</button>
  </li>
)

const ShoppingList = ({
  currentList,
  selectedTags,
  toggleItemCompletion,
  removeItem,
  showCompleted
}: {
  currentList: IList,
  selectedTags: ITag[],
  showCompleted: boolean,
  removeItem(item: IListItem): void,
  toggleItemCompletion(item: any): void
}) => {
  const hasSelectedTag = (item: IListItem) =>
    item.tags.some(
      itemTag => !!selectedTags.find(
        tag => tag.title === itemTag.title
      )
    )

  const listItems = currentList.items
  .filter(( item: IListItem ) => {
    const completion = item.completed === showCompleted
    const selection = selectedTags.length
      ? hasSelectedTag(item)
      : true
    return completion && selection
  })
  .map(( item: IListItem ) =>
      <ShoppingListItem
        key={ item.id }
        item={ item }
        removeItem={ removeItem }
        toggleItemCompletion={ toggleItemCompletion }
      />)
  return (
    <div className="shopping-list">
      { listItems.length
        ? <ul className="list">{ listItems }</ul>
        : <p>This list is empty 😱</p>
      }
    </div>
  )
}

const ShoppingListView = () => {
  const [ showCompleted, setShowCompleted ] = useState(false)
  const [ tags, setTags ] = useState([])
  const { actions, dispatch, state, showConfirm } = useContext(StoreContext)

  const handleRemoveItem = (item: IListItem) =>
    showConfirm(
      `Are you sure you want to delete ${item.title}?`,
      actions.removeListItem(item.id)
    )

  const handleToggleCompletion = (item: IListItem) =>
  dispatch(actions.updateListItem({
    ...item,
    completed: !item.completed
  }))

  return (
    <div className="shopping-list-view">
      <Collapsible title="Filters" collapsed={ true }>
        <TagList handleUpdateTags={ setTags }/>
      </Collapsible>
      <div>
        <input
          id="toggle_visibility"
          type="checkbox"
          checked={ showCompleted }
          onChange={ () => setShowCompleted(!showCompleted) }
        />
        <label htmlFor="toggle_visibility">Show Completed</label>
      </div>
      <hr />
      <ShoppingList
        currentList={ state.list }
        selectedTags={ tags }
        showCompleted={ showCompleted }
        toggleItemCompletion={ handleToggleCompletion }
        removeItem={ handleRemoveItem }
      />
    </div>
  )
}

export { ShoppingList, ShoppingListItem }

export default ShoppingListView
