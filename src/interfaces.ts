export interface IAction {
  type: string
  [key : string]: any
}

export interface IListItem {
  id: string
  title: string
  price: number
  tags: ITag[]
  isInvalidated: boolean
  completed: boolean
}

export interface IListItemUpdate {
  id?: string
  price?: number
  isInvalidated?: boolean
  tags?: ITag[]
  title?: string
  completed?: boolean
}

export interface IListItemData {
  id: string
  price: number
  title: string
  tags: ITag[]
  completed: boolean
}

export interface IList {
  id: string
  title: string
  items: IListItem[]
  isInvalidated: boolean
  isLoading: boolean
}

export interface IListUpdate {
  id: string
  newId?: string
  title?: string
  items?: IListItem[]
  isInvalidated?: boolean
  isLoading?: boolean
}

export interface IListData {
  id: string
  title: string
  items: string[]
}

export interface ITag {
  title: string
}

export interface IAppState {
  allLists: IList[]
  currentList?: IList
  showCompleted: boolean
  tags: ITag[]
  filters: {
    tags: ITag[]
    showCompleted: boolean
  }
}

