const uuid = require('uuid');

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
const delayRandom = () => delay(Math.random() * 1000)

const makeHttpResponse = (body, code) => ({ body, code })

const makeHttpSuccess = (body = 'OK!', code = 200) =>
  Promise.resolve(makeHttpResponse(body, code))

const makeHttpError = (body = 'An error occurred', code = 400) =>
  Promise.reject(makeHttpResponse(body, code))

const createEndpoint = (method, bindTo) => {
  return function () {
    const args = arguments
    return delayRandom()
      .then(() => method.apply(bindTo, args))
      .then(result => {
        bindTo.saveToDisk()
        return makeHttpSuccess(result)
      })
      .catch(error => makeHttpError(error))
  }
}

const randomInt = (min, max) =>
  Math.round((Math.random() * (max - min)) + min)

const randomItem = (array) =>
  array[randomInt(0, array.length - 1)]

const tags = [
  'Produce',
  'Meat/Poultry',
  'Natural Foods',
  'Snacks'
]

const backend = {
  _itemIdCounter: 0,
  _items: [],
  _listIdCounter: 0,
  _lists: [],
  loadFixtures() {
    for(var l=0; l<3; l++) {
      const list = this.addList({
        title: `List ${l}`
      })
      for(let i=0; i<10; i++) {
        this.addListItem(list.id, {
          completed: false,
          title: `Item ${i}`,
          price: (Math.random() * 10).toFixed(2),
          tags: [{ title: randomItem(tags) }]
        })
      }
    }
    return this;
  },
  loadFromDisk() {
    this._itemIdCounter = localStorage.getItem('itemIdCounter')
      || this._itemIdCounter
    this._listIdCounter = localStorage.getItem('listIdCounter')
      || this._listIdCounter
    this._items = JSON.parse(localStorage.getItem('items')) || [];
    this._lists = JSON.parse(localStorage.getItem('lists')) || [];
    return this;
  },
  saveToDisk() {
    localStorage.setItem('listIdCounter', this._listIdCounter);
    localStorage.setItem('itemIdCounter', this._itemIdCounter);
    localStorage.setItem('items', JSON.stringify(this._items));
    localStorage.setItem('lists', JSON.stringify(this._lists));
    return this;
  },
  _listContainsItem(listId, itemId) {
    return this._getList(listId)
      .then(list => list.items.includes(itemId))
  },
  _getList(listId) {
    const list = this._lists.find(l => l.id === listId);
    if (!list) {
      return Promise.reject(
        `No list with ID ${listId} exists`
      )
    }
    return Promise.resolve(list)
  },
  _getListItem(listItemId) {
    const item = this._items.find(i => i.id === listItemId)
    if (!item) {
      return Promise.reject(
        new Error(`No item with ID ${listItemId} exists`)
      )
    }
    return Promise.resolve(item)
  },

  // ---
  // Public Methods
  getAllLists() {
    return this._lists
  },
  getListItems(listId) {
    return this._getList(listId)
      .then(list => this._items.filter(i => list.items.includes(i.id)))
      .then(result => {
        return result
      })
  },
  getListWithItems(listId) {
    return Promise.all([
      this._getList(listId),
      this.getListItems(listId)
    ])
    .then(([ list, listItems ]) => {
      return {
        ...list,
        items: listItems
      }
    })
  },
  addList(listData) {
    const newList = {
      items: [],
      ...listData,
      id: uuid.v4()
    }
    this._lists.push(newList)
    return newList;
  },
  removeList(listId) {
    this._lists.filter(list => list.id === listId)
  },
  addListItem(listId, listItemData) {
    return this._getList(listId)
      .then(list => {
        const item = {
          ...listItemData,
          id: uuid.v4()
        }
        list.items.push(item.id);
        this._items.push(item);
        return item
      })
  },
  removeListItem(listId, listItemId) {
    return this._getList(listId)
      .then(list => {
        list.items = list.items.filter(item => item.id !== listItemId)
        this._items = this._items.filter(item => item.id !== listItemId)
      })
  },
  updateListItem(listItemId, listItemData) {
    return this._getListItem(listItemId)
      .then(listItem => {
         Object.assign(listItem, listItemData)
         return listItem
      })
  }
}

const frontend = {}
const methods = [
  'saveToDisk',
  'loadFromDisk',
  'loadFixtures',
  'getAllLists',
  'getListWithItems',
  'getListItems',
  'addList',
  'removeList',
  'addListItem',
  'removeListItem',
  'updateListItem'
].forEach(
  methodName => frontend[methodName] = createEndpoint(
    backend[methodName],
    backend
  )
)

export default frontend
